Pod::Spec.new do |spec|

  spec.name         = "PodsLibXD" # 私有库名称
  spec.version      = "1.1.1" # 私有库版本号
  spec.summary      = "PodsLibXD DY-framework" # 项目简介
  spec.description  = "这是一个pods私有库的基础组件的动态库 framework" # 项目简介

  spec.homepage     = "https://gitee.com/snilwx/dy-base-sdk" # 仓库的主页

  spec.license      = "MIT" # 开源许可证

  spec.author       = { "wangxiang" => "934416194@qq.com" } # 作者信息
  spec.platform     = :ios, "10.0" # 平台及支持的最低版本

  spec.source       = { :git => "https://gitee.com/snilwx/dy-base-sdk.git", :tag => "#{spec.version}" } #仓库地址
  
  #不会自动创建PodsLibDemo.bundle存放打包资源
  #spec.resources = 'PodsLibDemo/PodsLibDemo/Assets/*' # 导入资源文件
  
  #此处为需要添加的 framework 库
  spec.vendored_framework = 'PodsLibXD.framework'

  spec.requires_arc = true #是否支持ARC

end
