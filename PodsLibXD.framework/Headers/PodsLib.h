//
//  PodsLib.h
//  PodsLibDemo
//
//  Created by wangxiang on 2020/7/15.
//  Copyright © 2020 wangxiang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PodsLib : NSObject
- (NSString *)testPodsLib;

@end

NS_ASSUME_NONNULL_END
